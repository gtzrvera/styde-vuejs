// Podemos usar el EventBus para la comunicación de eventos entre componentes
let EventBus = new Vue;

// Forma para registrar un nuevo componente
Vue.component('app-icon', {
    template: '<span :class="cssClasses" aria-hidden="true"></span>',
    props: [
        'img'
    ],
    computed: {
        cssClasses: function() {
            return 'glyphicon glyphicon-' + this.img
        }
    }
});

Vue.component('app-task', {
    // Así referenciamos nuestro template que definimos en el tag <script>
    template: '#task-template',
    data: function() {
        return {
            editing: false,
            draft: ''
        };
    },
    created: function() {
        // Escuchamos el evento lanzado en el EventBus
        EventBus.$on('editing', index => {
            if (this.index != index) {
                this.discard();
            }
        });
    },
    props: [
        'task',
        'index'
    ],
    methods: {
        toggleStatus: function() {
            this.task.pending = !this.task.pending;
        },
        edit: function() {
            // Lanzamos el evento en el EventBus
            EventBus.$emit('editing', this.index);

            this.draft = this.task.description;
            this.editing = true;
        },
        update: function() {
            this.task.description = this.draft;
            this.editing = false;
        },
        discard: function() {
            this.editing = false;
        },
        remove: function() {
            // Con el método $emit podemos emitir un evento hacia el componente padre
            this.$emit('remove', this.index);
        }
    }
});

new Vue({
    el: '#app',
    data: {
        newTask: '',
        tasks: [{
            description: 'Aprender VueJS',
            pending: true,
        }, {
            description: 'Aprender Unty3D',
            pending: true,
        }, {
            description: 'Aprender PHP',
            pending: false,
        }]
    },
    //created: function() {
    //    this.tasks.forEach(task => this.$set(task, 'editing', false));
    //},
    methods: {
        createTask: function() {
            this.tasks.push({
                description: this.newTask,
                pending: true,
                editing: false
            });

            this.newTask = '';
        },
        deleteTask(index) {
            this.tasks.splice(index, 1);
        }
    }
});